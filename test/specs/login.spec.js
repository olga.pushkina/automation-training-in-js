import {describe, beforeEach, it} from "mocha";
import Pages from "../../pageobjects/pages/index.js";

import 'dotenv/config';
const credentials = process.env;

const {
    CORRECT_USERNAME,
    INCORRECT_USERNAME,
    CORRECT_PASSWORD
  } = credentials;
  

describe('Check login functionality of login form', async () => {

    beforeEach(async () => {
        await Pages.login.open();
    })

    it('Check user cannot login with no credentials', async () => {
        await Pages.login.username.click();
        await Pages.login.username.setValue(CORRECT_USERNAME);
        await Pages.login.password.setValue(CORRECT_PASSWORD);

        await Pages.login.username.click();
        await Pages.login.clearInputField();
        
        //await Pages.login.username.setValue('');
        //setValue('') does not work in my case, on my machine

        await Pages.login.loginButton.click();
        await expect(Pages.login.alert.window).toExist();
        await expect(Pages.login.alert.message).toHaveText(expect.stringContaining(
            'Epic sadface: Username is required'));
    })


    it('Check user cannot login with no password credential', async () => {
        await Pages.login.username.setValue(CORRECT_USERNAME);
        await Pages.login.password.setValue(CORRECT_PASSWORD);

        await Pages.login.password.click();
        await Pages.login.clearInputField();

        await Pages.login.loginButton.click();
        await expect(Pages.login.alert.window).toExist();
        await expect(Pages.login.alert.message).toHaveText( expect.stringContaining(
            'Epic sadface: Password is required'));
    })


    
        it('Check user can login with correct credentials', async () => {
            await Pages.login.username.setValue(CORRECT_USERNAME);
            await Pages.login.password.setValue(CORRECT_PASSWORD);
            await Pages.login.loginButton.click();

            await expect(Pages.inventory.logo).toExist();
        })
    

        it('Check user cannot login with incorrect credentials', async () => {
            await Pages.login.username.setValue(INCORRECT_USERNAME);
            await Pages.login.password.setValue(CORRECT_PASSWORD);
            await Pages.login.loginButton.click();

            await expect(Pages.login.alert.window).toExist();
        })
    
})

