import { Given, When, Then } from '@wdio/cucumber-framework';
import Pages from '../../pageobjects/pages/index.js';

Given('I open the login page', async () => {
    await Pages.login.open();
});

When(/^I enter username ([^"]*)$/, async (username) => {
    await Pages.login.username.setValue(username);
});

When(/^I enter password ([^"]*)$/, async (password) => {
    await Pages.login.password.setValue(password);
});

When(/^I clear the username field$/, async () => {
    await Pages.login.username.click();
    await Pages.login.clearInputField();
});

When(/^I clear the password field$/, async () => {
    await Pages.login.password.click();
    await Pages.login.clearInputField();
});

When(/^I click the login button$/, async () => {
    await Pages.login.loginButton.click();
});

Then(/^I should see an error message (.*)$/, async (message) => {
    await expect(Pages.login.alert.window).toExist();
    await expect(Pages.login.alert.message).toHaveText(expect.stringContaining(message));
});

Then(/^I expect to see the inventory page$/, async () => {
    
    await expect(Pages.inventory.logo).toExist();
});

Then(/^I should not login$/, async () => {
    await expect(Pages.login.alert.window).toExist();
});