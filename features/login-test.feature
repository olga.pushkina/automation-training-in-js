Feature: Login form testing

Background: Open login page 

  Given I open the login page

  Scenario Outline: Check user cannot login with no credentials

    When I enter username <username>
    And I enter password <password>
    And I clear the username field
    And I clear the password field
    And I click the login button
    Then I should see an error message <message>

    Examples:
      | username | password             | message                            |
      | tomsmith | SuperSecretPassword! | Epic sadface: Username is required |

  Scenario Outline: Check user cannot login with no password credential

    When I enter username <username>
    And I enter password <password>
    And I clear the password field
    And I click the login button
    Then I should see an error message <message>

    Examples:
      | username | password             | message                            |
      | tomsmith | SuperSecretPassword! | Epic sadface: Password is required |

  Scenario Outline: Check user can login with correct credentials

    When I enter username <username>
    And I enter password <password>
    And I click the login button
    Then I expect to see the inventory page 

    Examples:
      | username                | password             |
      | standard_user           | secret_sauce         |


  Scenario Outline: Check user cannot login with incorrect credentials

    When I enter username <username>
    And I enter password <password>
    And I click the login button
    Then I should not login

    Examples:
      | username                | password             |
      | locked_out_user         | secret_sauce         |
