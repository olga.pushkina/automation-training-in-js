# Final task

 **This project needs Node.js v20** 

- To start to work with the project clone this repository from https://gitlab.com/olga.pushkina/automation-training-in-js.git :
- In project directory run:
```sh
        npm init
```
- To install all dependensies from package.json run:
```sh
        npm i
```
- In order to run tests in Chrome and MicrosoftEgde please install its latest versions from official websites

- To run tests in Mocha
```sh
        npm run test:wdio
```
- To run BDD tests in Cucumber
```sh
        npm run test:cucumber
```
- In order to choose directly in which browser you would like to run tests, run 
```sh
        npm run test:wdio:chrome
```
- Or
```sh
        npm run test:wdio:edge
```
- Same with the Cucumber tests:
```sh
        npm run test:cucumber:chrome
```
- Or
```sh
        npm run test:cucumber:edge
```