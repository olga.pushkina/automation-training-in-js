export class AcceptedCredentials {
    get loginCredentials() {
        return $('#login_credentials');
    }

    get passwordCredentials() {
        return $('.login_password');
    }
}