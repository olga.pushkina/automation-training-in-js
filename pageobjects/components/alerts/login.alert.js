export class loginAlert {

    get window() {
        return $('[data-test="error-button"]');
    }
     
    get message() {
        return $('h3[data-test="error"]');

    }
}
