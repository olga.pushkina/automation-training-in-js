
import { loginAlert } from '../components/alerts/login.alert.js';
import Page from './page.js';

export class LoginPage extends Page {

    constructor() {
        super();
        this.alert = new loginAlert();
    }
    get username () {
        return $('[data-test="username"]');
    }

    get password () {
        return $('[data-test="password"]');
    }

    get loginButton () {
        return $('[data-test="login-button"]');
    }

    async clearInputField() {
        browser.performActions([{
            type: 'key',
            id: 'keyboard',
            actions: [{ type: 'keyDown', value: '\uE009' },
                { type: 'keyDown', value: 'a' },
                { type: 'keyUp', value: '\uE009' },
                { type: 'keyUp', value: 'a' },
                { type: 'keyDown', value: '\uE003' },
                { type: 'keyUp', value: '\uE003' }
            ]
          }]);
    }

    open () {
        return super.open("https://www.saucedemo.com/");
    }
}

