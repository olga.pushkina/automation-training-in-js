import {LoginPage} from "./login.page.js";
import {InventoryPage} from "./inventory.page.js";


class Pages {
    constructor() {
        this.login = new LoginPage();
        this.inventory = new InventoryPage();
    };
} 

export default new Pages();