import Page from "./page.js";

export class InventoryPage extends Page {

    get logo() {
        return $('[data-test="header-container"]');
    }
}