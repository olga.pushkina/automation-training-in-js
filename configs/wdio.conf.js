export const config = {
    
    runner: 'local',
    
    specs: [
        '.././test/specs/**/*.js',
        
    ],
  
    exclude: [
        // 'path/to/excluded/files'
    ],

    suites: {
        login: [
            '.././test/specs/**/*.js',
        ],
    },
    
    
    maxInstances: 6,
    
    capabilities: [
        {
            browserName: 'chrome',
            maxInstances: 3
        },
        {
            browserName: 'MicrosoftEdge',
            maxInstances: 3
        }
    ],

    
    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'info',
    
    bail: 0,
   
    waitforTimeout: 10000,
    
    connectionRetryTimeout: 120000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    
    framework: 'mocha',
    
    
    reporters: ['spec'],

    // Options to be passed to Mocha.
    // See the full list at http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000,
     //   parallel: true
    },

    }
